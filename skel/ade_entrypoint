#!/usr/bin/env bash
#
# Copyright 2017 - 2018 Ternaris
# SPDX-License-Identifier: Apache 2.0


set -e

if [[ -n "$GITLAB_CI" ]]; then
    exec "$@"
fi


if [[ -n "$DEBUG" ]]; then
    set -x
fi


if [[ -n "$TIMEZONE" ]]; then
    echo "$TIMEZONE" > /etc/timezone
    ln -sf /usr/share/zoneinfo/"$TIMEZONE" /etc/localtime
    dpkg-reconfigure -f noninteractive tzdata
fi


groupdel "$GROUP" &>/dev/null || true
groupadd -og "$GROUP_ID" "$GROUP"

# Setting the default shell here to Zsh has no effect that I know of, since
# `ade enter` uses Bash anyways.
useradd -M -u "$USER_ID" -g "$GROUP_ID" -d "/home/$USER" -s /bin/zsh "$USER"

groupdel video &> /dev/null || true
groupadd -og "${VIDEO_GROUP_ID}" video
gpasswd -a "${USER}" video


for x in /etc/skel/{,.}*; do
    target="/home/$USER/$(basename "$x")"
    if [[ ! -e "$target" && -e "$x" ]]; then
        cp -a "$x" "$target"
        chown -R "$USER":"$GROUP" "$target"
    fi
done

if [[ -z "$SKIP_ADEINIT" ]]; then
    for x in /opt/*; do
    if [[ -x "$x/.adeinit" ]]; then
        echo "Initializing $x"
        sudo -Hu "$USER" -- bash -lc "$x/.adeinit"
        echo "Initializing $x done"
    fi
    done


    ADEINIT="/home/$USER/.adeinit"
    if [[ -x "$ADEINIT" ]]; then
        cd "/home/$USER"
        sudo -EHu "$USER" -- bash -c "$ADEINIT" || echo "$ADEINIT exited with error $?"
        cd - > /dev/null
    fi
fi


echo 'ADE startup completed.'
exec "$@"

