
# ADE-UBUNTU

This image is designed to provide ADE with an Ubuntu experience as close as possible to a regular
user-interactive Ubuntu, with some essential developper tools preinstalled.

Use /home/.template/.adeinit to automatically setup your home directory upon startup, for instance
automatically cloning and building a project.

See https://ade-cli.readthedocs.io/ and https://gitlab.com/deb0ch/ade-ubuntu/ for more information.

Feel free to use this image as a base for your own images.
